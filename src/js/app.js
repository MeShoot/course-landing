var burger = document.getElementsByClassName('burger')[0];
var mainMenu = document.getElementsByClassName('main-menu')[0];
var close = document.getElementsByClassName('main-menu-close-btn')[0];

burger.addEventListener('click', function() {
    mainMenu.classList.add('active');
});

close.addEventListener('click', function() {
    mainMenu.classList.remove('active');
})